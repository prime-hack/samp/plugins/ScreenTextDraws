[![pipeline  status](https://gitlab.com/prime-hack/samp/plugins/ScreenTextDraws/badges/main/pipeline.svg)](https://gitlab.com/prime-hack/samp/plugins/ScreenTextDraws/-/commits/main)

# ScreenTextDraws

#### Plugin to take screen is only of TextDraws without game background.

## Activation

#### ALT+F2

## [Download](https://gitlab.com/prime-hack/samp/plugins/ScreenTextDraws/-/jobs/artifacts/main/download\?job\=win32)

