#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>
#include <SRHook.hpp>

class AsiPlugin : public SRDescent {
	SRHook::Hook<> renderTextdraws{ 0x9D929, 5, "samp" };

	size_t kid;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	bool takeScreen = false;
	SRTexture *texture = nullptr;

	void preRender();
	void postRender();

	void onKeyPressed( int key );
};

#endif // MAIN_H
