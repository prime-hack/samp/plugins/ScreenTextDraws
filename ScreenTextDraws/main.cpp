#include "main.h"
#include <samp_pimpl.hpp>
#include <SAMP/Chat.h>
#include <texture.h>
#include <callfunc.hpp>
#include <d3dx9.h>
#include <filesystem>

namespace fs = std::filesystem;
using namespace std::string_literals;

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	if ( SAMP::isR3() ) renderTextdraws.changeAddr( 0xC58D4 );
	renderTextdraws.onBefore += std::tuple{ this, &AsiPlugin::preRender };
	renderTextdraws.onAfter += std::tuple{ this, &AsiPlugin::postRender };
	renderTextdraws.install( 0, 0, false );

	kid = g_class.events->onKeyPressed += std::tuple{ this, &AsiPlugin::onKeyPressed };
}

AsiPlugin::~AsiPlugin() {
	g_class.events->onKeyPressed -= kid;
	renderTextdraws.remove();
	if ( texture ) g_class.draw->d3d9_releaseTexture( texture );
	SAMP::Chat::DeleteInstance();
}

void AsiPlugin::preRender() {
	if ( takeScreen ) {
		if ( !texture )
			texture = g_class.draw->d3d9_createTexture( g_class.params.BackBufferWidth, g_class.params.BackBufferHeight );
		else if ( texture->GetSize().x != g_class.params.BackBufferWidth || texture->GetSize().y != g_class.params.BackBufferHeight )
			texture->ReInit( g_class.params.BackBufferWidth, g_class.params.BackBufferHeight );

		texture->Begin();
		texture->Clear();
	}
}

void AsiPlugin::postRender() {
	if ( takeScreen && texture && texture->IsRenderToTexture() ) {
		takeScreen = false;
		CallFunc::ccall( 0x719840 ); // CFont::RenderFontBuffer

		texture->End();
		texture->Render( 0, 0 );

		fs::path screenPath( "textdraw_screens" );
		if ( !fs::exists( screenPath ) ) { fs::create_directories( screenPath ); }

		auto time = ::time( nullptr );
		auto lt = ::localtime( &time );
		char timeBuf[128];
		sprintf( timeBuf,
				 "%02d-%02d-%04d %02d.%02d.%02d",
				 lt->tm_mday,
				 lt->tm_mon + 1,
				 lt->tm_year + 1900,
				 lt->tm_hour,
				 lt->tm_min,
				 lt->tm_sec );

		auto screenName = screenPath / ( timeBuf + ".png"s );
		if ( fs::exists( screenName ) ) {
			int i = 2;
			while ( true ) {
				if ( !fs::exists( screenPath / ( timeBuf + "_"s + std::to_string( i ) + ".png" ) ) ) break;
				++i;
			}
			screenName = screenPath / ( timeBuf + "_"s + std::to_string( i ) + ".png" );
		}
		D3DXSaveTextureToFileA( screenName.string().c_str(), D3DXIFF_PNG, texture->GetTexture(), nullptr );
		SAMP::Chat::Instance()->addMsgInfo( "Save TextDraws screen to " + screenName.string() );
	}
}

void AsiPlugin::onKeyPressed( int key ) {
	if ( key == VK_F2 && g_class.events->isKeyDown( VK_LMENU ) ) takeScreen = true;
}
